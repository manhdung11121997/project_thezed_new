import React, { useEffect, useState, useRef } from "react";
import { Table, Modal, Button, Form, Pagination } from "react-bootstrap";
import Swal from "sweetalert2";
import axios from "axios";

export default function FoodManager() {
  const [apiFoodWithPage, setApiFoodWithPage] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);

  // than chieu den phan tu trong form thuoc tinh
  const formRef = useRef(null);
  const [selectedFood, setSelectedFood] = useState([]);
  const [totalPages, setTotalPages] = useState(1);
  const [keyword, setKeyWord] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [totalFoodElements, setTotalFoodElements] = useState(0);
  // Thay doi edit se render useEffect
  const [updateCounter, setUpdateCounter] = useState(0);

  /*** Function handle Show and Closes modal  */
  const handleShowModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
    setShowEditModal(false);
  };

  /*** Render API with page and search */
  useEffect(() => {
    let apiUrl = `http://localhost:8080/food/listFoodPaging?pageNo=${currentPage}`;
    if (keyword) {
      apiUrl = `http://localhost:8080/food/searchFoodPaging?keyword=${keyword}&pageNo=${currentPage}`;
    }
    axios.get(apiUrl).then((response) => {
      setApiFoodWithPage(response.data.content);
      setTotalFoodElements(response.data.totalElements);
      setCurrentPage(response.data.number + 1);
      setTotalPages(response.data.totalPages);
    });
  }, [currentPage, keyword, updateCounter]);

  /*** Function render API Food */
  const renderAPIFood = (food) => {
    return food.map((course) => (
      <tr key={course.foodID}>
        <td>
          <span className="custom-checkbox">
            <input type="checkbox" id="checkbox1" name="options[]" value="1" />
            <label htmlFor="checkbox1"></label>
          </span>
        </td>
        <td id="imageFoodList">
          <img
            src={`http://localhost:8080/food/${course.imageFoodDB}`}
            alt=""
          />
        </td>
        <td id="foodIDList">{course.foodID}</td>
        <td id="foodNameList">{course.foodName}</td>
        <td id="descriptionList">{course.description}</td>
        <td id="priceList">{course.price.toLocaleString("vi-VN") + "₫"}</td>
        <td id="menuNameList">{course.menuName}</td>
        <td>
          <a
            href="#editEmployeeModal"
            className="edit"
            data-bs-toggle="modal"
            onClick={() => handleEdit(course)}
          >
            <i className="fas fa-pencil-alt"></i>
          </a>
          <a
            href="#deleteEmployeeModal"
            className="delete"
            data-bs-toggle="modal"
            onClick={() => deleteFood(course.foodID)}
          >
            <i className="fas fa-trash-alt"></i>
          </a>
        </td>
      </tr>
    ));
  };

  /*** Fuction keyWord input */
  const handleKeyWord = (event) => {
    setKeyWord(event.target.value);
    setCurrentPage(1);
  };

  /*** Function add Food New */
  const handleSubmitAddFood = (event) => {
    event.preventDefault();
    // Get the values of the fields
    const formData = new FormData(formRef.current);
    // Check content value formData
    for (const [key, value] of formData.entries()) {
      console.log(key, value);
    }
    axios({
      method: "POST",
      url: "http://localhost:8080/food/addFood",
      data: formData,
      headers: { "Content-Type": "multipart/form-data" },
    }).then(() => {
      Swal.fire("Saved!", "Your work has been save", "success");
    });
    setShowModal(false);
  };

  /*** Delete product in Cart  */
  const deleteFood = (foodID) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .delete("http://localhost:8080/food/deleteFood", {
            params: { foodID: foodID },
          })
          .then(() => {
            const updatedApiFood = apiFoodWithPage.filter(
              (course) => course.foodID !== foodID
            );
            console.log("updateDelete", updatedApiFood);
            setApiFoodWithPage(updatedApiFood);
            Swal.fire("Deleted!", "Your file has been deleted.", "success");
          })
          .catch((error) => {
            console.error("Error deleting food:", error);
            Swal.fire(
              "Error!",
              "An error occurred while deleting the food.",
              "error"
            );
          });
      }
    });
  };

  /*** Function Edit Food */
  const handleEdit = (food) => {
    // Get value selected
    setSelectedFood(food);
    setShowEditModal(true);
  };

  /*** Function add update new to Food */
  const handleEditModal = (event) => {
    event.preventDefault();
    const formData = new FormData(formRef.current);
    const foodID = selectedFood.foodID;

    axios({
      method: "PUT",
      url: "http://localhost:8080/food/updateFood",
      params: {
        foodID: foodID,
      },
      data: formData,
      headers: { "Content-Type": "multipart/form-data" },
    }).then(() => {
      // getAPIFood();
      setUpdateCounter((prev) => prev + 1);
      setCurrentPage(1);
      Swal.fire("Saved!", "Your work has been update", "success");
    });
    setShowEditModal(false);
  };

  return (
    <div>
      <div className="col-md-12">
        <div className="table-responsive">
          <div className="table-wrapper">
            <div className="table-title">
              <div className="row">
                <div className="col-md-6">
                  <h2>
                    <i className="fas fa-utensils"></i>
                    <b>Đồ Ăn</b>
                  </h2>
                </div>
                <div className="col-md-4">
                  <a className="all" onClick={handleShowModal}>
                    <i className="fas fa-plus-circle"></i>
                    <span>Thêm đồ ăn mới</span>
                  </a>
                </div>
                {/* Modal Add Food New */}
                <Modal
                  show={showModal}
                  onHide={handleCloseModal}
                  id="addEmployeeModal"
                  centered
                >
                  <Modal.Header closeButton>
                    <Modal.Title id="addEmployeeModalLabel">Add</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <Form
                      id="formFood"
                      className="needs-validation"
                      ref={formRef}
                    >
                      {/* Nội dung của modal tại đây */}
                      <Form.Group>
                        <Form.Label htmlFor="foodName">Tên món ăn</Form.Label>
                        <Form.Text style={{ color: "red" }}>(*)</Form.Text>
                        <Form.Control
                          type="text"
                          id="foodName"
                          name="foodName"
                          required
                        />
                      </Form.Group>
                      <Form.Group>
                        <Form.Label htmlFor="description">Mô tả</Form.Label>
                        <Form.Text style={{ color: "red" }}>(*)</Form.Text>
                        <Form.Control
                          as="textarea"
                          id="description"
                          name="description"
                          required
                        />
                      </Form.Group>
                      <Form.Group>
                        <Form.Label htmlFor="price">Đơn giá</Form.Label>
                        <Form.Text style={{ color: "red" }}>(*)</Form.Text>
                        <Form.Control
                          type="text"
                          id="price"
                          name="price"
                          required
                        />
                      </Form.Group>
                      <Form.Group>
                        <Form.Label htmlFor="menuName">Tên menu</Form.Label>
                        <Form.Text style={{ color: "red" }}>(*)</Form.Text>
                        <Form.Select id="menuName" name="menuID" required>
                          <option selected disabled value="">
                            Choose...
                          </option>
                          <option value="1">Đặc Biệt</option>
                          <option value="2">Hải Sản</option>
                          <option value="3">Lai Rai</option>
                          <option value="4">Lẩu</option>
                          <option value="5">SaLad</option>
                          <option value="6">Món thịt</option>
                          <option value="7">Cơm, mì, cháo</option>
                          <option value="8">Nướng</option>
                        </Form.Select>
                      </Form.Group>
                      <Form.Group>
                        <Form.Label className="form-label">Hình ảnh</Form.Label>
                        <Form.Text style={{ color: "red" }}>(*)</Form.Text>
                        <br />
                        <Form.Control
                          type="file"
                          id="imageFood"
                          name="imageFood"
                          required
                        />
                      </Form.Group>
                    </Form>
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseModal}>
                      Close
                    </Button>
                    <Button
                      variant="primary"
                      type="submit"
                      form="formFood"
                      id="saveFood"
                      onClick={handleSubmitAddFood}
                    >
                      Save
                    </Button>
                  </Modal.Footer>
                </Modal>

                {/* Modal Edit Food New */}
                <Modal
                  show={showEditModal}
                  onHide={handleCloseModal}
                  id="addEmployeeModal"
                  centered
                >
                  <Modal.Header closeButton>
                    <Modal.Title id="addEmployeeModalLabel">Edit</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <Form
                      id="formFood"
                      className="needs-validation"
                      ref={formRef}
                    >
                      {/* Nội dung của modal tại đây */}
                      <Form.Group>
                        <Form.Label htmlFor="foodName">Tên món ăn</Form.Label>
                        <Form.Text style={{ color: "red" }}>(*)</Form.Text>
                        <Form.Control
                          type="text"
                          id="foodName"
                          name="foodName"
                          defaultValue={selectedFood.foodName}
                          required
                        />
                      </Form.Group>
                      <Form.Group>
                        <Form.Label htmlFor="description">Mô tả</Form.Label>
                        <Form.Text style={{ color: "red" }}>(*)</Form.Text>
                        <Form.Control
                          as="textarea"
                          id="description"
                          name="description"
                          defaultValue={selectedFood.description}
                          required
                        />
                      </Form.Group>
                      <Form.Group>
                        <Form.Label htmlFor="price">Đơn giá</Form.Label>
                        <Form.Text style={{ color: "red" }}>(*)</Form.Text>
                        <Form.Control
                          type="text"
                          id="price"
                          name="price"
                          defaultValue={selectedFood.price}
                          required
                        />
                      </Form.Group>
                      <Form.Group>
                        <Form.Label htmlFor="menuName">Tên menu</Form.Label>
                        <Form.Text style={{ color: "red" }}>(*)</Form.Text>
                        <Form.Select
                          id="menuName"
                          name="menuID"
                          defaultValue={selectedFood.menuName}
                          required
                        >
                          <option selected disabled value="">
                            Choose...
                          </option>
                          <option value="1">Đặc Biệt</option>
                          <option value="2">Hải Sản</option>
                          <option value="3">Lai Rai</option>
                          <option value="4">Lẩu</option>
                          <option value="5">SaLad</option>
                          <option value="6">Món thịt</option>
                          <option value="7">Cơm, mì, cháo</option>
                          <option value="8">Nướng</option>
                        </Form.Select>
                      </Form.Group>
                      <Form.Group>
                        <Form.Label className="form-label">Hình ảnh</Form.Label>
                        <Form.Text style={{ color: "red" }}>(*)</Form.Text>
                        <br />
                        <Form.Control
                          type="file"
                          id="imageFood"
                          name="imageFood"
                          defaultValue={selectedFood.imageFood}
                          required
                        />
                        <span>
                          <img
                            src={`http://localhost:8080/food/${selectedFood.imageFoodDB}`}
                            alt=""
                          />
                        </span>
                      </Form.Group>
                    </Form>
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseModal}>
                      Close
                    </Button>
                    <Button
                      variant="primary"
                      type="submit"
                      form="formFood"
                      id="saveFood"
                      onClick={handleEditModal}
                    >
                      Save Update
                    </Button>
                  </Modal.Footer>
                </Modal>
                {/* Search Food */}
                <div className="col-md-2">
                  <div className="search-box">
                    <i class="fas fa-search"></i>
                    <input
                      type="text"
                      className="form-control search"
                      placeholder="Search…"
                      onInput={handleKeyWord}
                    ></input>
                  </div>
                </div>
              </div>
            </div>
            {/* List Food */}
            <Table className="table table-bordered table-hover">
              <thead className="title-manager">
                <tr>
                  <th>
                    <span className="custom-checkbox">
                      <input type="checkbox" id="selectAll"></input>
                      <label htmlFor="selectAll"></label>
                    </span>
                  </th>
                  <th>Hình ảnh</th>
                  <th>Mã món ăn</th>
                  <th>Tên món ăn</th>
                  <th>Mô tả</th>
                  <th>Giá</th>
                  <th>Tên menu</th>
                  <th>Chức năng</th>
                </tr>
              </thead>
              <tbody id="list-food" className="list-manager">
                {renderAPIFood(apiFoodWithPage)}
              </tbody>
            </Table>
            {/* Pagination */}
            <div className="">
              <div className="hint-text">
                Showing <b>10</b> out of{" "}
                <b className="sum-entries">{totalFoodElements}</b> entries
              </div>
              <Pagination>
                <Pagination.Prev
                  disabled={currentPage === 1}
                  onClick={() => setCurrentPage((prevPage) => prevPage - 1)}
                />
                {Array.from({ length: totalPages }, (value, i) => (
                  <Pagination.Item
                    key={i}
                    active={currentPage === i + 1}
                    onClick={() => setCurrentPage(i + 1)}
                  >
                    {i + 1}
                  </Pagination.Item>
                ))}
                <Pagination.Next
                  disabled={currentPage === totalPages}
                  onClick={() => setCurrentPage((prevPage) => prevPage + 1)}
                />
              </Pagination>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
