import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";

function MenuFood(props) {
  /*** Get Location URL */
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);

  const selectedMenuFromUrl = queryParams.get("selectedMenu");
  const [selectedMenu, setSelectedMenu] = useState(
    selectedMenuFromUrl || "ĐẶC BIỆT"
  );

  useEffect(() => {
    setSelectedMenu(selectedMenuFromUrl || "ĐẶC BIỆT");
  }, [selectedMenuFromUrl]);



  /*** Set initial value  */
  // const [selectedMenu, setSelectedMenu] = useState("ĐẶC BIỆT");
  return (
    <div>
      {/* Detail Menu */}
      <div className="container car-detail-menu mt-5">
        <div className="box-menu">
          <div
            className={`col-md-1 box-menu-text ${
              selectedMenu === "ĐẶC BIỆT" ? "active" : ""
            }`}
            onClick={() => {
              setSelectedMenu("ĐẶC BIỆT")
            }}
          >
            ĐẶC BIỆT
          </div>
          <div
            className={`col-md-1 box-menu-text ${
              selectedMenu === "HẢI SẢN" ? "active" : ""
            }`}
            onClick={() => setSelectedMenu("HẢI SẢN")}
          >
            HẢI SẢN
          </div>
          <div
            className={`col-md-1 box-menu-text ${
              selectedMenu === "LAI RAI" ? "active" : ""
            }`}
            onClick={() => setSelectedMenu("LAI RAI")}
          >
            LAI RAI
          </div>
          <div
            className={`col-md-1 box-menu-text ${
              selectedMenu === "LẨU" ? "active" : ""
            }`}
            onClick={() => setSelectedMenu("LẨU")}
          >
            LẨU
          </div>
          <div
            className={`col-md-1 box-menu-text ${
              selectedMenu === "SALAD" ? "active" : ""
            }`}
            onClick={() => setSelectedMenu("SALAD")}
          >
            SALAD
          </div>
          <div
            className={`col-md-1 box-menu-text ${
              selectedMenu === "MÓN THỊT" ? "active" : ""
            }`}
            onClick={() => setSelectedMenu("MÓN THỊT")}
          >
            MÓN THỊT
          </div>
          <div
            className={`col-md-1 box-menu-text ${
              selectedMenu === "CƠM MÌ CHÁO" ? "active" : ""
            }`}
            onClick={() => setSelectedMenu("CƠM MÌ CHÁO")}
          >
            CƠM MÌ CHÁO
          </div>
          <div
            className={`col-md-1 box-menu-text ${
              selectedMenu === "NƯỚNG" ? "active" : ""
            }`}
            onClick={() => setSelectedMenu("NƯỚNG")}
          >
            NƯỚNG
          </div>
        </div>
        <div class="container mx-auto mt-4">
          <div className="row menu-food-image">
            {/* Call renderAPIFood */}
            {props.renderAPIFood(
              props.apiFood.filter((course) => course.menuName === selectedMenu)
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
export default MenuFood;
