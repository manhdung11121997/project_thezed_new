import React, { useEffect, useState, useMemo, useRef } from "react";
import { Form, Row, Col, Button, Modal } from "react-bootstrap";
import axios from "axios";

export default function PaymentFood(props) {
  const [cities, setCities] = useState([]);
  const [districts, setDistricts] = useState([]);
  const [wards, setWards] = useState([]);
  const [selectedCity, setSelectedCity] = useState("");
  const [selectedDistrict, setSelectedDistrict] = useState("");
  const [selectedWard, setSelectedWard] = useState("");

  const [showModal, setShowModal] = useState(false);
  const [apiBank, setApiBank] = useState([]);
  const [selectedTab, setSelectedTab] = useState("credit-card");

  const formCustomerRef = useRef("");
  //validate input
  const [validated, setValidated] = useState(false);

  const [validatedQR, setValidatedQR] = useState(false);

  /***  Seleced option API city VN */
  useEffect(() => {
    async function getData() {
      console.log("Render lien tuc");
      const response = await axios.get(
        "https://raw.githubusercontent.com/kenzouno1/DiaGioiHanhChinhVN/master/data.json"
      );
      setCities(response.data);
    }
    getData();
  }, []);

  useEffect(() => {
    if (selectedCity) {
      const city = cities.find((city) => city.Id === selectedCity);
      setDistricts(city.Districts);
      setSelectedDistrict(city.Districts[0].Id);
    } else {
      setDistricts([]);
      setSelectedDistrict("");
    }
    setWards([]);
  }, [selectedCity]);

  useEffect(() => {
    if (selectedDistrict) {
      const city = cities.find((city) => city.Id === selectedCity);
      const district = city.Districts.find(
        (district) => district.Id === selectedDistrict
      );
      if (district) {
        setWards(district.Wards);
      } else {
        setWards([]);
      }
    } else {
      setWards([]);
    }
  }, [selectedDistrict]);

  /*** Validate Form  */
  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    setValidated(true);
  };

  const handleSubmitQR = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    setValidatedQR(true);
  };

  /*** Render Cart */
  function RenderAPIOrderItem(apiCartFood) {
    return apiCartFood.map((course) => {
      return (
        <div>
          <tr>
            <td colspan="2" class="separator"></td>
          </tr>
          <tr class="d-flex justify-content-between text-content">
            <td class="product-food d-flex flex-row">
              <div class="product-foodName">{course.foodName}</div>
              <span>&nbsp;&nbsp;x&nbsp;&nbsp;</span>
              <div class="product-quantity">{course.quantity}</div>
            </td>
            <td class="subTotal-food">
              {course.total.toLocaleString("vi-VN") + "₫"}
            </td>
          </tr>
        </div>
      );
    });
  }

  useEffect(() => {
    console.log("RENDER1111");
    // Gọi API để lấy danh sách food khi vào trang Home
    axios
      .get("http://localhost:8080/bankCard/listBankCard")
      .then((response) => {
        const apiBankValue = response.data;
        console.log(apiBankValue);
        setApiBank(apiBankValue);
      });
  }, []);

  const [selectedBank, setSelectedBank] = useState();
  /*** Handle Select your bank  */
  const handleBank = (event) => {
    const selectedBankValue = parseInt(event.target.value);
    console.log("selectedBank", selectedBankValue);
    setSelectedBank(selectedBankValue);
  };

  /*** Function API Bank */
  const getAPIBank = (apiBank, selectedBank) => {
    return apiBank.map((course) => {
      if (selectedBank === course.bankID) {
        return (
          <div>
            <ul className="nav nav-tabs" id="myTab" role="tablist">
              <li className="nav-item" role="presentation">
                <a
                  className="nav-link activee"
                  id="visa-tab"
                  data-toggle="tab"
                  href="#visa"
                  role="tab"
                  aria-controls="visa"
                  aria-selected="true"
                >
                  <img
                    src={`http://localhost:8080/bankCard/${course.imageQRCode}`}
                    width="30"
                    alt="Bank Card"
                  />
                </a>
              </li>
            </ul>
            <div className="tab-content" id="myTabContent">
              <div
                className="tab-pane fade show active"
                id="visa"
                role="tabpanel"
                aria-labelledby="visa-tab"
              >
                <div className="bank-info">
                  <div className="bank-info-name">
                    <p>
                      <b>Tên:</b> {course.accountName}
                    </p>
                    <p>
                      <b>Số tài khoản:</b> {course.accountNumber}
                    </p>
                  </div>
                  <div className="bank-info-nameBank">
                    <p>
                      <b>Ngân hàng:</b> {course.nameBank}
                    </p>
                  </div>
                  <div className="bank-info-payment">
                    <p>
                      <b>Số tiền cần phải thanh toán:</b>{" "}
                      <span style={{ color: "red" }}>
                        {" "}
                        {props.totalPriceFinal}
                      </span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      }
    });
  };

  /*** Function handle Show and Closes modal  */
  const handleShowModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };
  const [imagePreviewUrl, setImagePreviewUrl] = useState("");

  const handleImageChange = (e) => {
    e.preventDefault();
    const reader = new FileReader();
    const file = e.target.files[0];

    reader.onloadend = () => {
      setImagePreviewUrl(reader.result);
    };

    if (file) {
      reader.readAsDataURL(file);
    }
  };
  /*** Handle fuction confirm Payment  */
  const handleConfirmPayment = () => {
    // alert("Cảm ơn bạn đã Mua hàng");
    // setShowModal(false);
  };

  /*** Handle fuction add Customer */
  const handleAddCustomer = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();

      const formData = new FormData(formCustomerRef.current);

      const city = formData.get("city");
      const district = formData.get("district");
      const ward = formData.get("wards");
      //Check Value Id
      const selectedCity = cities.find((item) => item.Id === city);
      const selectedDistrict = districts.find((item) => item.Id === district);
      const selectedWards = wards.find((item) => item.Id === ward);
      //Get Value Name
      const selectedNameCity = selectedCity.Name;
      const selectedNameDistrict = selectedDistrict.Name;
      const selectedNameWards = selectedWards.Name;

      const addressValue = formData.get("address");
      const address =
        addressValue +
        "," +
        selectedNameWards +
        "," +
        selectedNameDistrict +
        "," +
        selectedNameCity;

      console.log("Address", address);
      console.log("TOTALFINAL", props.totalPriceFinal.replace(/[^\d]/g, ""));
      const emailNew = formData.get("email");
      const callAPICustomer = props.apiCustomer.find(
        (item) => item.email === emailNew
      );

      if (callAPICustomer) {
        const customerID = callAPICustomer.customerID;
        axios
          .put("http://localhost:8080/customer/updateCustomer", null, {
            params: {
              customerID: customerID,
              fullName: formData.get("fullName"),
              phone: formData.get("phone"),
              address: address,
              totalAmount: props.totalPriceFinal.replace(/[^\d]/g, ""),
            },
          })
          .then((response) => {
            console.log(response.data);
          });
      } else {
        const formDataCustomer = {
          fullName: formData.get("fullName"),
          accountName: formData.get("accountName"),
          email: emailNew,
          phone: formData.get("phone"),
          address: address,
          totalAmount: props.totalPriceFinal.replace(/[^\d]/g, ""),
        };
        console.log("FORMDATA", formDataCustomer);
        axios
          .post("http://localhost:8080/customer/addCustomer", formDataCustomer)
          .then((response) => {
            console.log(response.data);
          });
      }
    }

    setValidated(true);
  };
  return (
    <div className="paymentFood">
      <div className="container ">
        <div className="py-5 text-center ">
          <h2>Đơn hàng của bạn</h2>
        </div>
        <div className="row">
          <div className="col-md-4 order-md-2 mb-4 ">
            <h4 className="d-flex justify-content-between align-items-center">
              <h4 className="mb-3">Giỏ hàng của bạn</h4>
              {/* <span className="badge badge-secondary badge-pill">3</span> */}
            </h4>

            <ul className="list-group mb-3">
              <table>
                <thead>
                  <tr className="d-flex justify-content-between text-heading">
                    <td>Sản phẩm</td>
                    <td>Tổng cộng</td>
                  </tr>
                </thead>
                <tbody id="payment-cartFood-show">
                  {/* Content of the table body will be dynamically generated */}
                  {RenderAPIOrderItem(props.apiDetailFood)}
                </tbody>
              </table>
              <hr className="my-3" />
              <div className="subTotal-food d-flex justify-content-between text-heading">
                <div className="subTotal-food-heading">
                  <h5>Tổng cộng</h5>
                </div>

                <div className="subTotal-food-price cartFood-total">
                  <h5>{props.totalPriceFormat}</h5>
                </div>
              </div>
              <hr className="my-3" />
              <div className="shipping-food d-flex justify-content-between text-heading">
                <div className="shipping-food-heading">
                  <h5>Phí vận chuyển</h5>
                </div>

                <div className="shipping-food-price cartFood-shipping">
                  <h5>{props.shippingPrice.toLocaleString("vi-VN") + "₫"}</h5>
                </div>
              </div>
              <hr className="my-3" />
              <div className="finalTotal-food d-flex justify-content-between text-heading">
                <div className="finalTotal-food-heading">
                  <h5>Tổng thanh toán</h5>
                </div>
                <div className="finalTotal-food-price cardFood-finalTotal">
                  <h5> {props.totalPriceFinal}</h5>
                </div>
              </div>

              <hr className="my-3" />
              <div className="payment-food-button">
                <button
                  type="submit"
                  form="form-check-payment"
                  className="btn btn-warning"
                  onClick={handleAddCustomer}
                >
                  Thanh toán
                </button>
              </div>
            </ul>
          </div>
          <div className="col-md-8 order-md-1">
            <h4 className="mb-3">Thông tin thanh toán</h4>
            <Form
              noValidate
              validated={validated}
              onSubmit={handleSubmit}
              id="form-check-payment"
              ref={formCustomerRef}
            >
              <Form.Group as={Col} md={12} controlId="fullName">
                <Form.Label>Họ Và Tên</Form.Label>
                <Form.Text style={{ color: "red" }}>(*)</Form.Text>
                <Form.Control
                  type="text"
                  className="form-control input-payment"
                  placeholder=""
                  name="fullName"
                  required
                />
                <Form.Control.Feedback type="invalid">
                  Vui lòng nhập họ và tên hợp lệ.
                </Form.Control.Feedback>
              </Form.Group>

              <Form.Group as={Col} md={12} controlId="username">
                <Form.Label>Tên tài khoản</Form.Label>
                <Form.Text style={{ color: "red" }}>(*)</Form.Text>
                <div className="input-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text icon-disabled">@</span>
                  </div>
                  <Form.Control
                    type="text"
                    className="form-control input-payment"
                    placeholder="Username"
                    name="accountName"
                    required
                    value={props.accountNameCustomer}
                    onChange={(event) =>
                      props.setAccountNameCustomer(event.target.value)
                    }
                  />
                  <Form.Control.Feedback type="invalid">
                    Tên người dùng của bạn là bắt buộc.
                  </Form.Control.Feedback>
                </div>
              </Form.Group>

              <Form.Group as={Col} md={12} controlId="email">
                <Form.Label>Email</Form.Label>
                <Form.Text style={{ color: "red" }}>(*)</Form.Text>
                <Form.Control
                  type="email"
                  className="form-control input-payment"
                  placeholder="manhdung11121997@gmail.com"
                  name="email"
                  required
                  value={props.emailCustomer}
                  onChange={(event) => {
                    props.setEmailCustomer(event.target.value);
                  }}
                />

                <Form.Control.Feedback type="invalid">
                  Vui lòng nhập địa chỉ email hợp lệ để cập nhật thông tin vận
                  chuyển.
                </Form.Control.Feedback>
              </Form.Group>

              <Form.Group as={Col} md={12} controlId="phone">
                <Form.Label>Số điện thoại</Form.Label>
                <Form.Text style={{ color: "red" }}>(*)</Form.Text>
                <Form.Control
                  type="number"
                  className="form-control input-payment"
                  name="phone"
                  required
                />
                <Form.Control.Feedback type="invalid">
                  Vui lòng nhập số điện thoại hợp lệ.
                </Form.Control.Feedback>
              </Form.Group>

              <Form.Group as={Col} md={12} controlId="address">
                <Form.Label>Địa chỉ</Form.Label>
                <Form.Text style={{ color: "red" }}>(*)</Form.Text>
                <Form.Control
                  type="text"
                  className="form-control input-payment"
                  placeholder="1234 Main St"
                  name="address"
                  required
                />
                <Form.Control.Feedback type="invalid">
                  Vui lòng nhập địa chỉ vận chuyển của bạn.
                </Form.Control.Feedback>
              </Form.Group>

              <Row>
                <Form.Group as={Col} md={4} controlId="city">
                  <Form.Label>Thành phố</Form.Label>
                  <Form.Text style={{ color: "red" }}>(*)</Form.Text>
                  <Form.Control
                    as="select"
                    className="form-select d-block w-100 input-payment"
                    required
                    value={selectedCity}
                    onChange={(e) => {
                      setSelectedCity(e.target.value);
                    }}
                    name="city"
                  >
                    <option value="" disabled>
                      Chọn tỉnh thành
                    </option>
                    {cities.map((city) => (
                      <option key={city.Id} value={city.Id}>
                        {city.Name}
                      </option>
                    ))}
                  </Form.Control>
                  <Form.Control.Feedback type="invalid">
                    Vui lòng chọn tỉnh thành.
                  </Form.Control.Feedback>
                </Form.Group>

                <Form.Group as={Col} md={4} controlId="district">
                  <Form.Label>Quận</Form.Label>
                  <Form.Text style={{ color: "red" }}>(*)</Form.Text>
                  <Form.Control
                    as="select"
                    className="form-select d-block w-100 input-payment"
                    required
                    value={selectedDistrict}
                    onChange={(e) => setSelectedDistrict(e.target.value)}
                    name="district"
                  >
                    <option value="" disabled>
                      Chọn quận huyện
                    </option>
                    {districts.map((district) => (
                      <option key={district.Id} value={district.Id}>
                        {district.Name}
                      </option>
                    ))}
                  </Form.Control>
                  <Form.Control.Feedback type="invalid">
                    Vui lòng chọn quận, huyện.
                  </Form.Control.Feedback>
                </Form.Group>

                <Form.Group as={Col} md={4} controlId="ward">
                  <Form.Label>Phường, Xã</Form.Label>
                  <Form.Text style={{ color: "red" }}>(*)</Form.Text>
                  <Form.Control
                    as="select"
                    className="form-select d-block w-100 input-payment"
                    required
                    value={selectedWard}
                    onChange={(e) => setSelectedWard(e.target.value)}
                    name="wards"
                  >
                    <option value="" disabled>
                      Chọn phường xã
                    </option>
                    {wards.map((ward) => (
                      <option key={ward.Id} value={ward.Id}>
                        {ward.Name}
                      </option>
                    ))}
                  </Form.Control>
                  <Form.Control.Feedback type="invalid">
                    Vui lòng chọn phường xã.
                  </Form.Control.Feedback>
                </Form.Group>
              </Row>

              <hr className="mb-4" />
            </Form>

            {/* Form Payment */}

            <div className="payment-cart-food" id="payment-cart-food">
              <h4 className="mb-3">Payment</h4>
              <div className="row">
                <div className="col-lg-12 mx-auto">
                  <div className="card">
                    <div className="card-header">
                      <div className="shadow-sm pt-4 pl-2 pr-2 pb-2">
                        <ul
                          className="nav nav-pills rounded nav-fill mb-3"
                          role="tablist"
                        >
                          <li className="nav-item">
                            <button
                              className={`nav-link ${
                                selectedTab === "credit-card" ? " active" : ""
                              }`}
                              onClick={() => setSelectedTab("credit-card")}
                            >
                              <i className="fas fa-credit-card mr-2"></i> Credit
                              Card
                            </button>
                          </li>
                          <li className="nav-item">
                            <button
                              className={`nav-link${
                                selectedTab === "paypal" ? " active" : ""
                              }`}
                              onClick={() => setSelectedTab("paypal")}
                            >
                              <i className="fab fa-paypal mr-2"></i> Paypal
                            </button>
                          </li>

                          <li className="nav-item">
                            <button
                              className={`nav-link${
                                selectedTab === "net-banking" ? " active" : ""
                              }`}
                              onClick={() => setSelectedTab("net-banking")}
                            >
                              <i className="fas fa-mobile-alt mr-2"></i> Net
                              Banking
                            </button>
                          </li>
                        </ul>
                      </div>

                      <div className="tab-content">
                        <div
                          id="credit-card"
                          className={`tab-pane fade${
                            selectedTab === "credit-card" ? " show active" : ""
                          } pt-3`}
                        >
                          {/* Nội dung của tab Credit Card */}
                          <div
                            id="credit-card"
                            className="tab-pane fade show active pt-3"
                          >
                            <form
                              role="form"
                              onSubmit={(event) => event.preventDefault()}
                            >
                              <div className="form-group">
                                <label htmlFor="username">
                                  <h6>Card Owner</h6>
                                </label>
                                <input
                                  type="text"
                                  name="username"
                                  autoFocus
                                  placeholder="Card Owner Name"
                                  required
                                  className="form-control input-payment"
                                />
                              </div>
                              <div className="form-group mt-3">
                                <label htmlFor="cardNumber">
                                  <h6>Card number</h6>
                                </label>
                                <div className="input-group">
                                  <div className="input-group-append">
                                    <span className="input-group-text text-muted">
                                      <i className="fab fa-cc-visa mx-1"></i>
                                      <i className="fab fa-cc-mastercard mx-1"></i>
                                      <i className="fab fa-cc-amex mx-1"></i>
                                    </span>
                                  </div>
                                  <input
                                    type="text"
                                    name="cardNumber"
                                    id="cardNumber"
                                    placeholder="Valid card number"
                                    className="form-control input-payment"
                                    required
                                  />
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-sm-8">
                                  <div className="form-group mt-3">
                                    <label>
                                      <span className="hidden-xs">
                                        <h6>Expiration Date</h6>
                                      </span>
                                    </label>
                                    <div className="input-group">
                                      <input
                                        type="number"
                                        placeholder="MM"
                                        name=""
                                        className="form-control input-payment"
                                        required
                                      />
                                      <input
                                        type="number"
                                        placeholder="YY"
                                        name=""
                                        className="form-control input-payment"
                                        required
                                      />
                                    </div>
                                  </div>
                                </div>
                                <div className="col-sm-4">
                                  <div className="form-group mb-4 mt-3">
                                    <label
                                      data-bs-toggle="tooltip"
                                      title="Three digit CV code on the back of your card"
                                    >
                                      <h6>
                                        CVV{" "}
                                        <i className="fa fa-question-circle d-inline"></i>
                                      </h6>
                                    </label>
                                    <input
                                      type="text"
                                      required
                                      className="form-control input-payment"
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="card-footer">
                                <button
                                  type="button"
                                  className="subscribe btn btn-primary btn-block shadow-sm"
                                >
                                  Confirm Payment
                                </button>
                              </div>
                            </form>
                          </div>
                        </div>

                        <div
                          id="paypal"
                          className={`tab-pane fade${
                            selectedTab === "paypal" ? " show active" : ""
                          } pt-3`}
                        >
                          {/* Nội dung của tab Paypal */}

                          <h6 className="pb-2">
                            Select your paypal account type
                          </h6>
                          <div className="form-group ">
                            <label className="radio-inline">
                              <input type="radio" name="optradio" checked />
                              Domestic
                            </label>
                            <label className="radio-inline ms-2 ">
                              <input type="radio" name="optradio" />
                              International
                            </label>
                          </div>
                          <p>
                            <button type="button" className="btn btn-primary">
                              <i className="fab fa-paypal mr-2"></i> Log into my
                              Paypal
                            </button>
                          </p>
                          <p className="">
                            Note: After clicking on the button, you will be
                            directed to a secure gateway for payment. After
                            completing the payment process, you will be
                            redirected back to the website to view details of
                            your order.
                          </p>
                        </div>

                        <div
                          id="net-banking"
                          className={`tab-pane fade${
                            selectedTab === "net-banking" ? " show active" : ""
                          } pt-3`}
                        >
                          {/* Nội dung của tab Net Banking */}
                          <div className="form-group">
                            <label htmlFor="Select Your Bank">
                              <h6>Select your Bank</h6>
                            </label>
                            <select
                              className="form-control input-payment"
                              id="payment-bankName"
                              onChange={handleBank}
                            >
                              <option value="" selected disabled>
                                --Vui lòng chọn Ngân hàng của bạn--
                              </option>
                              <option value="8">Vietcombank</option>
                              <option value="9">TPBank</option>
                              <option value="3">Sacombank</option>
                            </select>
                          </div>
                          <div className="form-group mt-3">
                            <p>
                              <button
                                type="button"
                                className="btn btn-primary"
                                data-bs-toggle="modal"
                                data-bs-target="#staticBackdrop"
                                onClick={handleShowModal}
                              >
                                <i className="fas fa-mobile-alt mr-2"></i>{" "}
                                Proceed Payment
                              </button>
                            </p>
                          </div>
                          <p>
                            Note: After clicking on the button, you will be
                            directed to a secure gateway for payment. After
                            completing the payment process, you will be
                            redirected back to the website to view details of
                            your order.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* Modal QR pay */}
            <Modal
              show={showModal}
              onHide={handleCloseModal}
              backdrop="static"
              keyboard={false}
            >
              <Modal.Header closeButton>
                <Modal.Title>QR CODE</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div id="payment-QRCode">
                  {/* Nội dung API Bank */}
                  {getAPIBank(apiBank, selectedBank)}
                </div>
                <div className="form-group bank-image">
                  <Form
                    noValidate
                    validated={validatedQR}
                    onSubmit={handleSubmitQR}
                    id="form-check-payment-QR"
                  >
                    <Form.Label htmlFor="payment-image">
                      Gửi ảnh chuyển khoản:
                    </Form.Label>

                    <div className="avatar-upload">
                      <div className="avatar-edit">
                        <Form.Control
                          type="file"
                          id="imageUpload"
                          accept=".png, .jpg, .jpeg"
                          required
                          onChange={handleImageChange}
                        />

                        <Form.Label htmlFor="imageUpload">
                          <i className="fas fa-cloud-upload-alt"></i>
                        </Form.Label>
                        <Form.Control.Feedback
                          type="invalid"
                          className="error-QR"
                        >
                          Vui lòng gởi ảnh chuyển khoản.
                        </Form.Control.Feedback>
                      </div>

                      <div className="avatar-preview">
                        {imagePreviewUrl ? (
                          <div
                            id="imagePreview"
                            style={{
                              backgroundImage: `url(${imagePreviewUrl})`,
                            }}
                          ></div>
                        ) : (
                          <div id="imagePreview"></div>
                        )}
                      </div>
                    </div>
                  </Form>
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="secondary" onClick={handleCloseModal}>
                  Close
                </Button>
                <Button
                  variant="primary"
                  type="submit"
                  onClick={handleConfirmPayment}
                  form="form-check-payment-QR"
                >
                  Xác nhận
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
        </div>
      </div>
    </div>
  );
}
