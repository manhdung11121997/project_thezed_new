import React, { useState, useRef, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Alert } from "react-bootstrap";
import axios from "axios";

export default function LoginTheZed(props) {
  const [selectedTab, setSelectedTab] = useState("form-center-login");

  const formLoginRef = useRef("");
  const formRegisterRef = useRef("");
  const [error, setError] = useState(null);
  const navigate = useNavigate();
  const [apiCustomer, setApiCustomer] = useState("");
  const [showSuccess, setShowSuccess] = useState("");

  useEffect(() => {
    axios
      .get("http://localhost:8080/customer/listCustomer")
      .then((response) => {
        setApiCustomer(response.data);
      });
  }, []);

  /*** Handele Register */
  const userRegister = (event) => {
    event.preventDefault();
    const formData = new FormData(formRegisterRef.current);

    const formDataRegister = {
      accountName: formData.get("accountName"),
      email: formData.get("email"),
      password: formData.get("password"),
    };
    console.log(formDataRegister);
    axios
      .post("http://localhost:8080/customer/addCustomer", formDataRegister)
      .then(() => {
        axios
          .get("http://localhost:8080/customer/listCustomer")
          .then((response) => {
            setApiCustomer(response.data);
            setShowSuccess(true);
            setTimeout(() => setShowSuccess(false), 2000);
            setSelectedTab("form-center-login");
            formRegisterRef.current.reset();
          });
      });
  };

  /*** Handle Login */
  const userLogin = (event) => {
    event.preventDefault();
    const formData = new FormData(formLoginRef.current);

    const emailData = formData.get("email");
    const passwordData = formData.get("password");

    apiCustomer.map((course) => {
      if (course.email === emailData && course.password === passwordData) {
        props.setEmailCustomer(course.email);
        props.setAccountNameCustomer(course.accountName);
        //Chuyen tipe duong dan
        navigate("/");
        props.setIsLoggedIn(true);
      } else {
        setError("Email hoặc mật khẩu không đúng");
      }
    });
  };

  /*** Close Error */
  const handleClose = () => {
    setError(null);
  };

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-md-6 appAside">
          {/* Use CSS to set max-width for the image */}
          <img
            src="/images/WelcomeTheZedNew1.jpg"
            alt=""
            style={{ maxWidth: "100%", height: "100vh" }}
          />
        </div>

        <div className="col-md-6 appForm" style={{ height: "100vh" }}>
          <div className="pageSwitcher">
            <button
              className={`pageSwitcherItem  ${
                selectedTab === "form-center-login" ? " active" : ""
              }`}
              onClick={() => setSelectedTab("form-center-login")}
            >
              Đăng nhập
            </button>
            <button
              className={`pageSwitcherItem me-5 ${
                selectedTab === "form-center-register" ? " active" : ""
              }`}
              onClick={() => setSelectedTab("form-center-register")}
            >
              Đăng Ký
            </button>
          </div>
          <div className="Error-Succer">
            {error && (
              <Alert className="custom-alert" onClose={handleClose} dismissible>
                {error}
              </Alert>
            )}
            {showSuccess && (
              <Alert
                variant="success"
                className="custom-success"
                onClose={handleClose}
                dismissible
              >
                Đăng ký thành công! Vui lòng đăng nhập để tiếp tục.
              </Alert>
            )}
          </div>

          <div className="tab-content">
            {/* Đăng nhập */}

            <div
              className={`formCenter ${
                selectedTab === "form-center-login" ? "show active" : ""
              }`}
              id="form-center-login"
            >
              <div className="formTitle">
                <div
                  className={`formTitleLink  ${
                    selectedTab === "form-center-login" ? " active" : ""
                  }`}
                  onClick={() => setSelectedTab("form-center-login")}
                >
                  Đăng nhập
                </div>{" "}
              </div>
              <form className="formFields" ref={formLoginRef}>
                {/* Your Sign In form content */}
                <div className="formField">
                  <label className="formFieldLabel" htmlFor="email">
                    E-Mail Address
                  </label>
                  <input
                    type="email"
                    id="email"
                    className="formFieldInput"
                    placeholder="Enter your email"
                    name="email"
                    required
                  />
                </div>

                <div className="formField">
                  <label className="formFieldLabel" htmlFor="password">
                    Password
                  </label>
                  <input
                    type="password"
                    id="password"
                    className="formFieldInput"
                    placeholder="Enter your password"
                    name="password"
                    required
                  />
                </div>

                <div className="formField">
                  <button
                    type="submit"
                    className="formFieldButton"
                    onClick={userLogin}
                  >
                    Sign In
                  </button>
                  <Link to="/" className="formFieldLink">
                    Create an account
                  </Link>
                </div>
              </form>
            </div>
            {/* Đăng ký */}

            <div
              id="form-center-register"
              className={`formCenter ${
                selectedTab === "form-center-register" ? "show active" : ""
              }`}
            >
              <div className="formTitle">
                <div
                  className={`formTitleLink  ${
                    selectedTab === "form-center-login" ? " active" : ""
                  }`}
                  onClick={() => setSelectedTab("form-center-login")}
                >
                  Đăng ký
                </div>{" "}
              </div>

              <form className="formFields" ref={formRegisterRef}>
                {/* Your Sign Up form content */}

                <div className="formField">
                  <label className="formFieldLabel" htmlFor="accountName">
                    Full Name
                  </label>
                  <input
                    type="text"
                    id="accountName"
                    className="formFieldInput"
                    placeholder="Enter your full name"
                    name="accountName"
                  />
                </div>
                <div className="formField">
                  <label className="formFieldLabel" htmlFor="password">
                    Password
                  </label>
                  <input
                    type="password"
                    id="password"
                    className="formFieldInput"
                    placeholder="Enter your password"
                    name="password"
                  />
                </div>
                <div className="formField">
                  <label className="formFieldLabel" htmlFor="email">
                    E-Mail Address
                  </label>
                  <input
                    type="email"
                    id="email"
                    className="formFieldInput"
                    placeholder="Enter your email"
                    name="email"
                  />
                </div>

                <div className="formField">
                  <label className="formFieldCheckboxLabel">
                    <input
                      className="formFieldCheckbox"
                      type="checkbox"
                      name="hasAgreed"
                    />{" "}
                    I agree all statements in{" "}
                    <a href="null" className="formFieldTermsLink">
                      terms of service
                    </a>
                  </label>
                </div>
                <div className="formField">
                  <button
                    type="submit"
                    className="formFieldButton"
                    onClick={userRegister}
                  >
                    Sign Up
                  </button>{" "}
                  <Link to="/sign-in" className="formFieldLink">
                    I'm already a member
                  </Link>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
