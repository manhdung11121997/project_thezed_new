import React from "react";
import { Link, Outlet } from "react-router-dom";

export default function HeaderFood(props) {
  return (
    <div>
      <header>
        <div className="container">
          <div className="wrap">
            <div className="row">
              <div className="col-md-6 d-flex align-items-center">
                <p className="mb-0 phone pl-md-2">
                  <a href="#" className="mr-2">
                    <span className="fa fa-phone me-1"></span> 035 9188 975{" "}
                  </a>
                  <a href="#">
                    <span className="fa fa-paper-plane me-1"></span>{" "}
                    thezed.grillandbeer@gmail.com
                  </a>
                </p>
              </div>
              <div className="col-md-6 d-flex justify-content-md-end">
                <div className="social-media mr-4">
                  <p className="mb-0 d-flex">
                    <a
                      href="#"
                      className="d-flex align-items-center justify-content-center"
                    >
                      <span className="fa fa-facebook">
                        <i className="sr-only">Facebook</i>
                      </span>
                    </a>
                    <a
                      href="#"
                      className="d-flex align-items-center justify-content-center"
                    >
                      <span className="fa fa-twitter">
                        <i className="sr-only">Twitter</i>
                      </span>
                    </a>
                    <a
                      href="#"
                      className="d-flex align-items-center justify-content-center"
                    >
                      <span className="fa fa-instagram">
                        <i className="sr-only">Instagram</i>
                      </span>
                    </a>
                    <a
                      href="#"
                      className="d-flex align-items-center justify-content-center"
                    >
                      <span className="fa fa-dribbble">
                        <i className="sr-only">Dribbble</i>
                      </span>
                    </a>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <nav>
            <div className="row">
              <ul className="icon-list">
                <div className="box-logo">
                  <img
                    src="/images/logo/logo2.jpg"
                    alt=""
                    className="header-image"
                  />
                </div>
                <div className="box-search">
                  <button
                    type="button"
                    className="btn btn-danger dropdown-toggle"
                    data-bs-toggle="dropdown"
                  >
                    <span id="search_concept">Filter by</span>{" "}
                    <span className="caret"></span>
                  </button>
                  <ul className="dropdown-menu dropdown-menu-right">
                    <li>
                      <a className="dropdown-item" href="#">
                        Contains
                      </a>
                    </li>
                    <li>
                      <a className="dropdown-item" href="#">
                        It's equal
                      </a>
                    </li>
                    <li>
                      <a className="dropdown-item" href="#">
                        Greater than
                      </a>
                    </li>
                    <li>
                      <a className="dropdown-item" href="#">
                        Less than
                      </a>
                    </li>
                  </ul>
                  <div className="input-group">
                    <div id="search-autocomplete" className="form-outline">
                      <input
                        type="search"
                        id="form-search"
                        className="form-control"
                        placeholder="Search term..."
                      />
                    </div>
                    <button type="button" className="btn btn-danger">
                      <i className="fas fa-search"></i>
                    </button>
                  </div>
                </div>
                <div className="box-icon">
                  <li className="dropdown menu-item rounded-icon logout">
                    {props.isLoggedIn ? (
                      <img src="/images/teamwork.jpg" alt="" />
                    ) : (
                      <i className="fa fa-user" style={{ color: "white" }}></i>
                    )}
                    <ul className="dropdown-menu logout">
                      <li className="menu-item-login">
                        <Link to="/login">Tài khoản của tôi</Link>
                      </li>
                      <li className="menu-item-login">
                        <Link to="/login">Đăng Xuất</Link>
                      </li>
                    </ul>
                  </li> 

                  {/* {/* </li> */}
                  {/* <div class="dropdown">
                    <a
                      class="btn btn-secondary dropdown-toggle"
                      href="#"
                      role="button"
                      id="dropdownMenuLink"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      UserName
                    </a>

                    <ul
                      class="dropdown-menu"
                      aria-labelledby="dropdownMenuLink"
                    >
                      <li>
                        <a class="dropdown-item" href="#">
                          Hồ sơ
                        </a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="#">
                          Cài đặt
                        </a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="#">
                          Đăng xuất
                        </a>
                      </li>
                    </ul>
                  </div> */}
                  <li className="rounded-icon">
                    <a
                      href="#"
                      data-bs-toggle="modal"
                      data-bs-target="#cartFoodModal"
                      onClick={props.handleCartIconClick}
                    >
                      <i class="fa fa-shopping-cart"></i>
                      <span id="cartItemCount" className="cart-item-count ">
                        {props.countFoodName}
                      </span>
                    </a>
                  </li>
                </div>
              </ul>
            </div>
            <div className="row-menu">
              <ul className="row-menu-nav">
                <li className="menu-item">
                  <Link to="/">GIỚI THIỆU</Link>
                </li>

                <li className="dropdown menu-item">
                  <a href="#">THỰC ĐƠN</a>
                  <ul className="dropdown-menu">
                    <li>
                      <Link to="MenuFood">Danh mục</Link>
                    </li>
                    <li>
                      <Link to="DetailFood?foodID=5">Chi Tiết</Link>
                    </li>
                  </ul>
                </li>
                <li className="dropdown menu-item">
                  <a href="#">QUẢN LÝ</a>
                  <ul className="dropdown-menu">
                    <li>
                      <Link to="FoodManager">Menu</Link>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </header>
      <Outlet />
    </div>
  );
}
