package com.example.projectthezed.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.projectthezed.model.OrderItemsDTO;
import com.example.projectthezed.service.OrderItemService;

// TODO: Auto-generated Javadoc
/**
 * The Class OrderItemsRestController.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
@RestController
@CrossOrigin
@RequestMapping("/orderItems")
public class OrderItemsRestController {

    /** The order item service. */
    @Autowired
    OrderItemService orderItemService;

    /**
     * Adds the order item.
     *
     * @param orderItemsDTO the order items DTO
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping("/addOrderItems")
    public ResponseEntity<String> addOrderItem(@RequestBody OrderItemsDTO orderItemsDTO) throws Exception {
        orderItemService.addOrderItems(orderItemsDTO);
        return ResponseEntity.ok(" successfully");
    }

    /**
     * Show order item.
     *
     * @return the response entity
     */
    @GetMapping("/listOrderItems")
    public ResponseEntity<List<OrderItemsDTO>> showOrderItem(){
        List<OrderItemsDTO> list = orderItemService.getAllOrderItems();
        System.err.println(list.toString());
        if (list.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    /**
     * Delete order item.
     *
     * @param orderItemsID the order items ID
     * @return the response entity
     */
    //API delete Food
    @DeleteMapping(value = "/deleteOrderItems")
    public ResponseEntity<String> deleteOrderItem(@RequestParam Integer orderItemsID) {
        orderItemService.deleteOrderItems(orderItemsID);
        return ResponseEntity.ok("Delete successfully");
    }

    /**
     * Update order item.
     *
     * @param orderItemsDTO the order items DTO
     * @param orderItemsID the order items ID
     * @return the response entity
     */
    @PutMapping(value = "/updateOrderItems")
    public ResponseEntity<String> updateOrderItem(@ModelAttribute OrderItemsDTO orderItemsDTO, @RequestParam Integer orderItemsID ) {
        orderItemService.updateOrderItems(orderItemsDTO, orderItemsID);
        return ResponseEntity.ok("Delete successfully");
    }

}
