package com.example.projectthezed.controller;

import com.example.projectthezed.entity.Customer;
import com.example.projectthezed.model.CustomerDTO;
import com.example.projectthezed.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class CustomerController.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
@RestController
@CrossOrigin
@RequestMapping("/customer")
public class CustomerController {

    /** The customer service. */
    @Autowired
    CustomerService customerService;

    /**
     * Adds the customer.
     *
     * @param customerDTO the customer DTO
     * @return the response entity
     */
    @PostMapping("/addCustomer")
    public ResponseEntity<String> addCustomer(@RequestBody  CustomerDTO customerDTO){
        System.err.println(customerDTO.toString());
        customerService.addCustomer(customerDTO);
        return ResponseEntity.ok("Add successfully");
    }
    @GetMapping("/listCustomer")
    public ResponseEntity<List<Customer>> showListCustomer() {
        List<Customer> list = customerService.getAllCustomer();
        if (list.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }
    @PutMapping(path = "/updateCustomer")
    public ResponseEntity<String> updateCustomer(@RequestBody CustomerDTO customerDTO, @RequestParam("customerID") Integer customerID) throws Exception {

        System.out.println("Update"+ customerDTO.toString());
        customerService.updateCustomer(customerDTO, customerID);
        return ResponseEntity.ok("Update successfully");
    }



}
