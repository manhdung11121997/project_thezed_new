package com.example.projectthezed.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.projectthezed.model.FoodDTO;
import com.example.projectthezed.service.FoodService;
import com.example.projectthezed.service.impl.FoodServiceImpl;

// TODO: Auto-generated Javadoc
/**
 * The Class FoodsRestController.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
@RestController
@CrossOrigin
@RequestMapping("/food")
public class FoodsRestController {
    
    /** The food service impl. */
    @Autowired
    FoodServiceImpl foodServiceImpl;
    
    /** The food service. */
    @Autowired
    FoodService foodService;

    /**
     * Adds the food.
     *
     * @param foodDTO the food DTO
     * @return the response entity
     * @throws Exception the exception
     */
    // API lay tu client ve add vao DB
    @PostMapping("/addFood")
    public ResponseEntity<String> addFood(@ModelAttribute FoodDTO foodDTO) throws Exception {
        foodServiceImpl.uploadFood(foodDTO);
        System.err.println(foodDTO.toString());

        return ResponseEntity.ok("Image uploaded successfully");
    }

    /**
     * Download image.
     *
     * @param foodName the food name
     * @return the response entity
     */
    // API lay hinh anh hien thi client
    @GetMapping("/{foodName}")
    public ResponseEntity<?> downloadImage(@PathVariable String foodName) {
        byte[] imageData = foodServiceImpl.downloadImage(foodName);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.valueOf("image/png"))
                .body(imageData);
    }

    /**
     * Show get list.
     *
     * @return the response entity
     */
    // API lay Food hien thi len client
    @GetMapping(path = "/listFood")
    public ResponseEntity<List<FoodDTO>> showGetList() {
        List<FoodDTO> list = foodService.getAllFoods();
        if (list.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }
    
    /**
     * Show get list paging food.
     *
     * @param pageNo the page no
     * @param pageSize the page size
     * @return the response entity
     */
    //GetAll Food with Paging
    @GetMapping(path = "/listFoodPaging")
    public ResponseEntity<Page<FoodDTO>> showGetListPagingFood(
    		@RequestParam(defaultValue = "1") int pageNo,
				@RequestParam(defaultValue = "10") int pageSize) {
    
    	Page<FoodDTO> products = foodService.getAllFoodPaging(pageNo, pageSize);
		return ResponseEntity.ok(products);
     
    }
    
    /**
     * Search food.
     *
     * @param keyword the keyword
     * @param pageNo the page no
     * @param pageSize the page size
     * @return the response entity
     */
    @GetMapping(path = "/searchFoodPaging")
    public ResponseEntity<Page<FoodDTO>> searchFood(
    		@RequestParam String keyword,
    		@RequestParam(defaultValue = "1") int pageNo,
				@RequestParam(defaultValue = "10") int pageSize) {
    	Page<FoodDTO> result = foodService.searchFoodWithPagination(keyword, pageNo, pageSize);
        return ResponseEntity.ok(result);
     
    }
    
    
    /**
     * Update foods.
     *
     * @param foodDTO the food DTO
     * @param foodID the food ID
     * @return the response entity
     * @throws Exception the exception
     */
    //API update Food
    @PutMapping(path = "/updateFood")
    public ResponseEntity<String> updateFoods(@ModelAttribute FoodDTO foodDTO, @RequestParam("foodID") Integer foodID) throws Exception {
        foodServiceImpl.updateFood(foodDTO, foodID);
        return ResponseEntity.ok("Update successfully");
    }

    /**
     * Delete.
     *
     * @param foodID the food ID
     * @return the response entity
     */
    //API delete Food
    @DeleteMapping(value = "/deleteFood")
    public ResponseEntity<String> delete(@RequestParam Integer foodID) {
        foodServiceImpl.deleteFood(foodID);
        return ResponseEntity.ok("Delete successfully");
    }
}


