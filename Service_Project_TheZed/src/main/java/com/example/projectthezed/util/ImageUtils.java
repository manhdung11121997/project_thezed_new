package com.example.projectthezed.util;

import java.io.ByteArrayOutputStream;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

// TODO: Auto-generated Javadoc
/**
 * The Class ImageUtils.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
public class ImageUtils {
    
    /**
     * Compress image.
     *
     * @param data the data
     * @return the byte[]
     */
    public static byte[] compressImage (byte[] data) {
        Deflater deflater = new Deflater();
        deflater.setLevel (Deflater.BEST_COMPRESSION);
        deflater.setInput(data);
        deflater. finish();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] tmp = new byte[4*1024];
        while (!deflater. finished()) {
            int size = deflater.deflate(tmp); outputStream.write(tmp,  0, size);
        }
        try {
            outputStream.close();
        } catch (Exception ignored) {
        }
        return outputStream.toByteArray();
    }

    /**
     * Decompress image.
     *
     * @param data the data
     * @return the byte[]
     */
    public static byte[] decompressImage(byte[] data) {
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] tmp = new byte[4*1024];
        try {
             while (!inflater. finished()) {
                int count = inflater.inflate(tmp); outputStream.write(tmp,  0, count);
            }
            outputStream.close();
        } catch (Exception ignored) {
        }
        return outputStream.toByteArray();
    }
}
