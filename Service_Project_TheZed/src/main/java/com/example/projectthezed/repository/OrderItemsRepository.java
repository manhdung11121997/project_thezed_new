package com.example.projectthezed.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.projectthezed.entity.OrderItems;

// TODO: Auto-generated Javadoc
/**
 * The Interface OrderItemsRepository.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
public interface OrderItemsRepository  extends JpaRepository<OrderItems, Integer> {

    /**
     * Find all by food.
     *
     * @return the list
     */
    @Query(value = "SELECT f.imageFood, f.foodName, f.price, o.quantity, o.total, o.orderItemID " +
            "FROM Food f " +
            "JOIN OrderItems o ON o.food.foodID = f.foodID")
    List<Object[]> findAllByFood();
}
