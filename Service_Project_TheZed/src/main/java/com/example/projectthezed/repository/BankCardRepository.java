package com.example.projectthezed.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.projectthezed.entity.BankCard;

// TODO: Auto-generated Javadoc
/**
 * The Interface BankCardRepository.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
@Repository
public interface BankCardRepository extends JpaRepository<BankCard, Integer>  {

    /**
     * Find by name bank.
     *
     * @param nameBank the name bank
     * @return the optional
     */
    Optional<BankCard> findByNameBank(String nameBank);


}
