package com.example.projectthezed.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.projectthezed.entity.Food;
import com.example.projectthezed.entity.Menu;
import com.example.projectthezed.model.FoodDTO;
import com.example.projectthezed.repository.FoodRepository;
import com.example.projectthezed.repository.FoodRespositoryPaging;
import com.example.projectthezed.repository.MenuRepository;
import com.example.projectthezed.service.FoodService;

// TODO: Auto-generated Javadoc
/**
 * The Class FoodServiceImpl.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
@Service
public class FoodServiceImpl implements FoodService {
    
    /** The food repository. */
    @Autowired
    FoodRepository foodRepository;
    
    /** The menu repository. */
    @Autowired
    MenuRepository menuRepository;
    
    /** The food respository paging. */
    @Autowired
    FoodRespositoryPaging foodRespositoryPaging;

    /**
     * Gets the all foods.
     *
     * @return the all foods
     */
    //Lay getAll Food
    @Override
    public List<FoodDTO> getAllFoods() {
        List<Object[]> results = foodRepository.findAllByMenuName();
        System.out.println(results);
        List<FoodDTO> foods = new ArrayList<>();
        //Set to DB food
        for (Object[] result : results) {
            FoodDTO food = new FoodDTO();
            food.setFoodID((int) result[0]);
            food.setPrice((int) result[1]);
            food.setDescription((String) result[2]);
            food.setFoodName((String) result[3]);
            food.setImageFoodDB((String) result[4]);
            food.setMenuName((String) result[5]);
            foods.add(food);
        }
        return foods;

    }

    /**
     * Update food.
     *
     * @param dto the dto
     * @param foodID the food ID
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    public void updateFood(FoodDTO dto, Integer foodID) throws IOException {
        Optional<Food> userFromDb = foodRepository.findById(foodID);
        System.err.println("FoodDTO" + dto);
        // crush the variables of the object found
        if (userFromDb.isPresent()) {
            Food food = userFromDb.get();
            MultipartFile imageFile = dto.getImageFood();
            // TH. check imageFood from Client has isEmpty not, create new image
            if (imageFile != null && !imageFile.isEmpty()) {
                //random image
                String fileExtension = FilenameUtils.getExtension(imageFile.getOriginalFilename());
                String generatedFileName = UUID.randomUUID().toString().replace("-", "");
                generatedFileName = generatedFileName + "." + fileExtension;
                String UPLOAD_DIRECTORY = "src/main/resources/static/image/";
                Files.copy(imageFile.getInputStream(), Paths.get(UPLOAD_DIRECTORY + File.separator + generatedFileName));
                // Delete the previous image
                deleteImage(food.getImageFood());
                // Set to DB
                food.setImageFood(generatedFileName);
            }
            food.setFoodName(dto.getFoodName());
            food.setPrice(dto.getPrice());
            food.setDescription(dto.getDescription());
            // Set menu properties
            Menu menu = menuRepository.getReferenceById(dto.getMenuID());
            System.err.println("MENU" + menu);
            food.setMenu(menu);

            // save to DB
            foodRepository.save(food);
        } else {
            System.err.println("Insert không thành công");
        }
    }

    /**
     * Delete food.
     *
     * @param foodID the food ID
     */
    //Xu ly delete Food
    @Override
    public void deleteFood(Integer foodID){
        foodRepository.deleteById(foodID);
    }

    /**
     * Delete image.
     *
     * @param imageName the image name
     */
    //Xu ly delete image trong Service
    private void deleteImage(String imageName) {
        String UPLOAD_DIRECTORY = "src/main/resources/static/image/";
        String filePath = UPLOAD_DIRECTORY + File.separator + imageName;
        File file = new File(filePath);
        if (file.delete()) {
            System.err.println("Previous image deleted successfully.");
        } else {
            System.err.println("Failed to delete the previous image.");
        }
    }

    /**
     * Upload food.
     *
     * @param dto the dto
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    //Add Food vao DB
    public String uploadFood(FoodDTO dto) throws IOException {
        //lay phan tu image tai len tu doi tuong dto
        String fileExtension = FilenameUtils.getExtension(dto.getImageFood().getOriginalFilename());
        String generatedFileName = UUID
                .randomUUID()
                .toString()
                .replace("-", "");
        generatedFileName = generatedFileName + "." + fileExtension;
        String UPLOAD_DIRECTORY = "src/main/resources/static/image/";
        Files.copy(dto.getImageFood().getInputStream(), Paths.get(UPLOAD_DIRECTORY + File.separator + generatedFileName));
        //Set doi tuong menuId
        Menu menu = menuRepository.getReferenceById(dto.getMenuID());
        Food foods = foodRepository.save(Food.builder()
                .imageFood(generatedFileName)
                .foodName(dto.getFoodName())
                .price(dto.getPrice())
                .menu(menu)
                .description(dto.getDescription())
                .build());

        if (dto != null) {
            return "file upload succesfully : " + dto.getImageFood().getOriginalFilename();
        }
        return null;
    }
    
    /**
     * Download image.
     *
     * @param foodName the food name
     * @return the byte[]
     */
    //Lay hinh anh tu Service hien thi cho Client
    public byte[] downloadImage(String foodName) {
        Optional<Food> dbImageData = foodRepository.findByFoodName(foodName);
        String UPLOAD_DIRECTORY = "src/main/resources/static/image/";
        Path path = Paths.get(UPLOAD_DIRECTORY + foodName);
        byte[] imageBytes = new byte[0];
        try {
            imageBytes = Files.readAllBytes(path);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return imageBytes;
    }

	/**
	 * Gets the all food paging.
	 *
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @return the all food paging
	 */
	@Override
	public Page<FoodDTO> getAllFoodPaging(int pageNo, int pageSize) {
		int adjustedPageNo = pageNo - 1; // Giảm 1 để bắt đầu từ 0
	    Pageable pageable = PageRequest.of(adjustedPageNo, pageSize);
	    Page<Object[]> foodPage = foodRespositoryPaging.findAllFoodWithMenuName(pageable);

	    List<FoodDTO> foods = new ArrayList<>();
	    for (Object[] result : foodPage.getContent()) {
	        FoodDTO food = new FoodDTO();
	        food.setFoodID((int) result[0]);
	        food.setPrice((int) result[1]);
	        food.setDescription((String) result[2]);
	        food.setFoodName((String) result[3]);
	        food.setImageFoodDB((String) result[4]);
	        food.setMenuName((String) result[5]);
	        foods.add(food);
	    }
	    	//used to Contructor of Spring Data, 
	    //Foods: danh sach cac phan tu hien tai
	    //Pageable: Trang hien tai
	    return new PageImpl<>(foods, pageable, foodPage.getTotalElements());
	}
	
	/**
	 * Search food with pagination.
	 *
	 * @param keyword the keyword
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @return the page
	 */
	@Override
	public Page<FoodDTO> searchFoodWithPagination(String keyword, int pageNo, int pageSize) {
		int adjustedPageNo = pageNo - 1; // Giảm 1 để bắt đầu từ 0
	    Pageable pageable = PageRequest.of(adjustedPageNo, pageSize);
	    Page<Object[]> foodPage = foodRespositoryPaging.searchFoodWithMenuName(keyword, pageable);

	    List<FoodDTO> foods = new ArrayList<>();
	    for (Object[] result : foodPage.getContent()) {
	        FoodDTO food = new FoodDTO();
	        food.setFoodID((int) result[0]);
	        food.setPrice((int) result[1]);
	        food.setDescription((String) result[2]);
	        food.setFoodName((String) result[3]);
	        food.setImageFoodDB((String) result[4]);
	        food.setMenuName((String) result[5]);
	        foods.add(food);
	    }

	    return new PageImpl<>(foods, pageable, foodPage.getTotalElements());
	}
}
	


