package com.example.projectthezed.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.projectthezed.entity.Customer;
import com.example.projectthezed.entity.Invoices;
import com.example.projectthezed.model.CustomerDTO;
import com.example.projectthezed.repository.CustomerRespository;
import com.example.projectthezed.repository.InvoicesResponsitory;
import com.example.projectthezed.service.CustomerService;

// TODO: Auto-generated Javadoc
/**
 * The Class CustomerServiceImlp.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
@Service
public class CustomerServiceImlp implements CustomerService {
    
    /** The customer respository. */
    @Autowired
    CustomerRespository customerRespository;
    
    /** The invoices responsitory. */
    @Autowired
    InvoicesResponsitory invoicesResponsitory;


    /**
     * Adds the customer.
     *
     * @param customerDTO the customer DTO
     */
    @Override
    public void addCustomer(CustomerDTO customerDTO) {
        Customer customer = new Customer();
        customer.setFullName(customerDTO.getFullName());
        customer.setAccountName(customerDTO.getAccountName());
        customer.setEmail(customerDTO.getEmail());
        customer.setPhone(customerDTO.getPhone());
        customer.setAddress(customerDTO.getAddress());
        customer.setPassword(customerDTO.getPassword());
        // set TotalAmount to Invoices
        Invoices invoices = new Invoices();
        invoices.setTotalAmount(customerDTO.getTotalAmount());
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
        String formattedDate = dateFormat.format(calendar.getTime());
        invoices.setInvoicesID("HD".concat(formattedDate));
        invoices.setCustomer(customer);
        List<Invoices> invoicesList = new ArrayList<>();
        invoicesList.add(invoices);
        customer.setInvoices(invoicesList);
        customerRespository.save(customer);

    }
    /**
     * Get All the customer.
     *
     */
    @Override
    public List<Customer> getAllCustomer() {
        return  customerRespository.findAll();
    }

    /**
     * Update the customer.
     *
     * @param customerDTO the customer DTO
     * @param CustomerID the CustomerID
     */
    @Override
    public void updateCustomer( CustomerDTO customerDTO, Integer CustomerID) {
        Optional<Customer> customerOptional = customerRespository.findById(CustomerID);
        if(customerOptional.isPresent()){
            Customer customer = customerOptional.get();
            customer.setFullName(customerDTO.getFullName());
            customer.setPhone(customerDTO.getPhone());
            customer.setAddress(customerDTO.getAddress());
            //set update Invoice
            Invoices invoices = new Invoices();
            invoices.setTotalAmount(customerDTO.getTotalAmount());
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            String formattedDate = dateFormat.format(calendar.getTime());
            invoices.setInvoicesID("HD".concat(formattedDate));
            invoices.setCustomer(customer);
            List<Invoices> invoicesList = new ArrayList<>();
            invoicesList.add(invoices);
            customer.setInvoices(invoicesList);
            customerRespository.save(customer);
        }else{
            System.out.println("Update không thành công");
        }
    }
}