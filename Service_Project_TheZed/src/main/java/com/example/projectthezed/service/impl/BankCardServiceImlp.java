package com.example.projectthezed.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.projectthezed.entity.BankCard;
import com.example.projectthezed.model.BankCardDTO;
import com.example.projectthezed.repository.BankCardRepository;
import com.example.projectthezed.service.BankCardService;

// TODO: Auto-generated Javadoc
/**
 * The Class BankCardServiceImlp.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
@Service
public class BankCardServiceImlp implements BankCardService {

    /** The bank card repository. */
    @Autowired
    BankCardRepository bankCardRepository;
    
    /**
     * Adds the bank card.
     *
     * @param bankCardDTO the bank card DTO
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    public void addBankCard(BankCardDTO bankCardDTO) throws IOException {
        //lay phan tu image tai len tu doi tuong dto
        String fileExtension = FilenameUtils.getExtension(bankCardDTO.getImageQRCode().getOriginalFilename());
        String generatedFileName = UUID
                .randomUUID()
                .toString()
                .replace("-", "");
        generatedFileName = generatedFileName + "." + fileExtension;
        String UPLOAD_DIRECTORY = "src/main/resources/static/image/";
        Files.copy(bankCardDTO.getImageQRCode().getInputStream(), Paths.get(UPLOAD_DIRECTORY + File.separator + generatedFileName));

        BankCard bankCard = new BankCard();
        bankCard.setBankID(bankCardDTO.getBankID());
        bankCard.setNameBank(bankCardDTO.getNameBank());
        bankCard.setImageQRCode(generatedFileName);
        bankCard.setAccountName(bankCardDTO.getAccountName());
        bankCard.setAccountNumber(bankCardDTO.getAccountNumber());

        bankCardRepository.save(bankCard);

    }
    
    /**
     * Gets the all bank card.
     *
     * @return the all bank card
     */
    @Override
    public List<BankCard> getAllBankCard() {
        List<BankCard> bankCardList = bankCardRepository.findAll();
        return bankCardList;
    }
    
    /**
     * Show image bank.
     *
     * @param nameImageBank the name image bank
     * @return the byte[]
     */
    @Override
    public byte[] showImageBank(String nameImageBank) {
        Optional<BankCard> dbImageData = bankCardRepository.findByNameBank(nameImageBank);
        String UPLOAD_DIRECTORY = "src/main/resources/static/image/";
        Path path = Paths.get(UPLOAD_DIRECTORY + nameImageBank);
        byte[] imageBytes = new byte[0];
        try {
            imageBytes = Files.readAllBytes(path);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return imageBytes;
    }

}
