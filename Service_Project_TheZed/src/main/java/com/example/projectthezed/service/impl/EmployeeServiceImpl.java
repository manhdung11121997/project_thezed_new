package com.example.projectthezed.service.impl;

import com.example.projectthezed.entity.Employee;
import com.example.projectthezed.repository.EmployeeRepository;
import com.example.projectthezed.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
// TODO: Auto-generated Javadoc

/**
 * The Class EmployeeServiceImpl.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {
    
    /** The employee repository. */
    @Autowired
    EmployeeRepository employeeRepository;

    /**
     * Gets the all employee.
     *
     * @return the all employee
     */
    @Override
    public List<Employee> getAllEmployee() {
        return employeeRepository.findAll();
    }

    /**
     * Adds the employee.
     *
     * @param employee the employee
     * @return the employee
     */
    @Override
    public Employee addEmployee(Employee employee) {
        employeeRepository.save(employee);
        return employee;
    }
}
