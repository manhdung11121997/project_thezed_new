package com.example.projectthezed.service;

import com.example.projectthezed.entity.Customer;
import com.example.projectthezed.model.CustomerDTO;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Interface CustomerService.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
public interface CustomerService {

    /**
     * Adds the customer.
     *
     * @param customerDTO the customer DTO
     */
    void addCustomer(CustomerDTO customerDTO);

    /**
     * Get list the customer.
     *
     */
    List<Customer> getAllCustomer();

    /**
     * Update the customer.
     *
     * @param customerDTO the customer DTO
     * @param CustomerID the CustomerID
     */
    void updateCustomer(CustomerDTO customerDTO, Integer CustomerID );
}
