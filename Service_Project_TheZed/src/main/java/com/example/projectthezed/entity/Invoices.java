package com.example.projectthezed.entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Invoices {
    @Id
    private String invoicesID;
    private int totalAmount;
    private String paymentStatus;
    private String paymentMethod;
    private String deliveryStatus;
    @ManyToOne()
    @JoinColumn(name = "customerID")
    private Customer customer;
    @ManyToOne()
    @JoinColumn(name = "orderItemID")
    private OrderItems orderItems;
    @ManyToOne()
    @JoinColumn(name = "reservationID")
    private Reservations reservations;
    @ManyToOne()
    @JoinColumn(name = "bankID")
    private BankCard bankCard;
}
