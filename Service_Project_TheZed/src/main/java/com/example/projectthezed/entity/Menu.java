package com.example.projectthezed.entity;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "menu")
public class Menu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "menuID" )
    private int menuID;
    @Column(columnDefinition = "nvarchar(255)", name = "menuName" )
    private String menuName;
    @Column(columnDefinition = "nvarchar(500)", name = "description")
    private String description;
    @Column(columnDefinition = "nvarchar(255)", name = "imageMenu")
    private String imageMenu;
    @OneToMany(mappedBy = "menu")
    private List<Systems> systems;
    @OneToMany(mappedBy = "menu")
    private List<Food> food;


}
